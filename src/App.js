import React from 'react';
import './App.css';
import About from './components/About';
import Banner from './components/Banner';
// import FooterNew from './components/FooterNew';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import Skills from './components/Skills';
import Work from './components/Work';
import Projects from './components/Projects';
import Footer from "./components/Footer";
import ProjectsPageHeader from './components/ProjectsPageHeader';
import ProjectsPageFooter from './components/ProjectsPageFooter';

import ScrollToTop from "./components/ScrollToTop";
import Testimonial from './components/Testimonial';

function App() {
  return (
      <div className="app">
        <Router>
          <ScrollToTop />
          <Switch>
            <Route path='/projects'>
              {/* <Header /> */}
              <ProjectsPageHeader />
              <Projects />
              <ProjectsPageFooter />
              {/* <Footer /> */}
            </Route>

            <Route path='/'>
              <Header />
              <div className="appBannerAbout">
                <Banner />
                <About />
              </div>
              <Skills />
              <Work />
              {/* <Projects /> */}
              <Testimonial />
              <Footer />
            </Route>
          </Switch>
        </Router>
      </div>
  );
}

export default App;
