import React from 'react';
import "./Work.css";
// import WorkCard from '../cards/WorkCard';
// import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useHistory } from 'react-router-dom';
import Fade from 'react-reveal/Fade';
// import Projects from './Projects';
import WorkCard from '../cards/WorkCard';

function Work() {

    const history = useHistory();

    return (
        <div className="work">
            <div className="work__fadeTop" />
            <div className="work__heading">
                <Fade top>
                <h1>My Work</h1>
                </Fade>
            </div>

            <div className="workmain__container">
                <div className="work__container">
                    <Fade bottom>
                    <div className="work__containerLeft">
                        <img src="/images/work.png" alt="" />
                    </div>
                    </Fade>
                    <Fade bottom>
                    <div className="work__containerRight">
                        <img src="/images/workImg1.png" alt="" className="workImage" />
                        <p>
                            Here are some of my best projects I have worked on recently <br /> 
                            on the basis of my learning experience.
                        </p>
                        {/* <button onClick={() => history.push('./projects')}>Explore My Projects</button> */}
                    </div>
                    </Fade>
                </div>

                <div className="work__containerProjects">
                    <WorkCard 
                        src="/images/b2b.png"
                        href="https://justa1.web.app"
                        text="Just A1"
                    />
                    <WorkCard 
                        src="/images/snapup.png"
                        href="https://clone-dfd1a.web.app/"
                        text="Snap Up"
                    />
                    <WorkCard 
                        src="/images/tracker.png"
                        href="https://covid-19-tracker-ivory.vercel.app"
                        text="Covid-19 tracker"
                    />
                </div>

                <div className="work__btn">
                    <button className="exploreBtn" onClick={() => history.push('./projects')}>Explore More Projects</button>
                </div>
            </div>
    
            <div className="work__fadeBottom" />
        </div>
    )
}

export default Work;
