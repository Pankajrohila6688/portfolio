import React from 'react';
import ProjectCard from '../cards/ProjectCard';
// import CardRight from '../cards/CardRight';
// import Header from './Header';
// import Footer from './Footer';
import './Projects.css';
import Fade from 'react-reveal/Fade';
// import Zoom from 'react-reveal/Zoom';

function Projects() {

    return (
        <div className="project">
            <Fade>
            <div className="project__heading">
                {/* <div className="project__headingLine" /> */}
                <h1><span>Projects I've Built</span></h1>
            </div>
            </Fade>

            <div className="projectCards">
                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard 
                    projectCard="cardLeft" cardDetail="cardLeftDetail" cardHeading="cardLeftHeading" cardLang="cardLeftLang" cardLogo="cardLeftLogo"
                    projectImage="/images/b2b.png"
                    projectName="JustA1"
                    projectDetail="This is a business to business application, 
                    in which a user can Sign up or Login and can register their business in a particular category by filling a form, 
                    they can also edit and delete their business. Other users can contact to the business owner as well"
                    lng1="React"
                    lng2="Context api"
                    lng3="Firebase"
                    githubLink="https://bitbucket.org/Pankajrohila6688/justa1/src/master/"
                    liveDemo="https://justa1.web.app"
                />
                </Fade>
                {/* </Zoom> */}

                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard 
                    projectCard="cardRight" cardDetail="cardRightDetail" cardHeading="cardRightHeading" cardLang="cardRightLang" cardLogo="cardRightLogo"
                    projectImage="/images/snapup.png"
                    projectName="Snap Up"
                    projectDetail="This is an ecommerce application in which 
                    a user can create an account by email and password, 
                    there is a payment gateway using stripe by which 
                    a user can pay from their card using stripe and the information of the user will be stored in the cloud firestore"
                    lng1="Firebase"
                    lng2="Context api"
                    lng3="React"
                    githubLink="https://bitbucket.org/Pankajrohila6688/snap-up/src/master/"
                    liveDemo="https://clone-dfd1a.web.app/"
                />
                </Fade>
                {/* </Zoom> */}

                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard 
                    projectCard="cardLeft" cardDetail="cardLeftDetail" cardHeading="cardLeftHeading" cardLang="cardLeftLang" cardLogo="cardLeftLogo"
                    projectImage="/images/tracker.png"
                    projectName="Covid-19 tracker"
                    projectDetail="This is a covid-19 tracker which tells us the 
                    current statistics of covid around the world, 
                    there is a dropdown which has the list of all the countries around the world, we can select any of the country, 
                    selected country will be shown in the map and the covid-19 stats will be displayed"
                    lng1="React"
                    lng2="external api"
                    lng3="leaflet map"
                    githubLink="https://bitbucket.org/Pankajrohila6688/covid-19-tracker/src/master/"
                    liveDemo="https://covid-19-tracker-ivory.vercel.app"
                />
                </Fade>
                {/* </Zoom> */}

                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard
                    projectCard="cardRight" cardDetail="cardRightDetail" cardHeading="cardRightHeading" cardLang="cardRightLang" cardLogo="cardRightLogo"
                    projectImage="/images/mailer.png"
                    projectName="Mailer"
                    projectDetail="This is a dummy email sending application 
                    build with react and firebase which have a google authentication of firebase, 
                    we can login and can compose an email to anyone which will later be displayed in the application"
                    lng1="Firebase"
                    lng2="Redux"
                    lng3="React"
                    githubLink="https://bitbucket.org/Pankajrohila6688/mailer/src/master/"
                    liveDemo="https://clone-facd8.web.app"
                />
                </Fade>
                {/* </Zoom> */}

                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard 
                    projectCard="cardLeft" cardDetail="cardLeftDetail" cardHeading="cardLeftHeading" cardLang="cardLeftLang" cardLogo="cardLeftLogo"
                    projectImage="/images/groupy.png"
                    projectName="Groupy"
                    projectDetail="This is a group chatting application having 
                    google authentication using firebase, different users can login and can 
                    make diffrent channels or rooms in which they can chat with one another, 
                    limitless number of users can join the same room"
                    lng1="React"
                    lng2="Redux"
                    lng3="firebase"
                    githubLink="https://bitbucket.org/Pankajrohila6688/groupy/src/master/"
                    liveDemo="https://discord-clone-chi.vercel.app"
                />
                </Fade>
                {/* </Zoom> */}
                
                {/* <Zoom> */}
                <Fade bottom>
                <ProjectCard
                    projectCard="cardRight" cardDetail="cardRightDetail" cardHeading="cardRightHeading" cardLang="cardRightLang" cardLogo="cardRightLogo"
                    projectImage="/images/hols1.png"
                    projectName="Hols"
                    projectDetail="This application is completely based on react which has a list of different places, 
                    It has a calender feature by which we can choose the number of guests and select any date, month and year after searching
                     a user will be redirected to a different page by react routing"
                    lng1=""
                    lng2=""
                    lng3="React"
                    githubLink="https://bitbucket.org/Pankajrohila6688/hols/src/master/"
                    liveDemo="https://mystifying-lovelace-8021c4.netlify.app"
                />
                </Fade>
                {/* </Zoom> */}
            </div>
        </div>
    )
}

export default Projects;
