import React, { useState } from 'react';
import "./Footer.css";
import GitHubIcon from '@material-ui/icons/GitHub';
// import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { Link } from "react-scroll";
// import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Fade from 'react-reveal/Fade';
import emailjs from 'emailjs-com';
import EmailIcon from '@material-ui/icons/Email';
import { useHistory } from "react-router-dom";

function Footer() {

    const [contact, setContact] = useState(false);

    const history = useHistory();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");


    const sendEmail = (e) => {

        e.preventDefault();    //This is important, i'm not sure why, but the email won't send without it

        emailjs.sendForm('service_91mdlis', 'template_5ynv2sw', e.target, 'user_ZKRQ2M4aWbcCtN8SLQxJd')
          .then((result) => {
            console.log(result.text); //  window.location.reload()  //This is if you still want the page to reload (since e.preventDefault() cancelled that behavior) 
          }, (error) => {
              console.log(error.text);
          });
        console.log('Email sent')
        alert("Email Sent to Pankaj Rohila")
    
        setName("")
        setEmail("")
        setMessage("")

    }

    return (
        <div className="footer">
            <Fade top>
            <div className="newFooter__heading">
                <h1><span>GET IN TOUCH</span></h1>
                <p>PANKAJ ROHILA</p>
            </div>
            </Fade>

            <Fade top>
            <div className="newFooter__content">
                {/* <Fade left> */}
                <div className="newFooter__contentLeft">
                    <Link spy={true} smooth={true} to="banner"><p className="newFooter__contentLeftLink">HOME</p></Link>
                    <Link spy={true} smooth={true} to="testimonial"><p className="newFooter__contentLeftLink">TESTIMONIALS</p></Link>
                    <Link spy={true} smooth={true} to="skills"><p className="newFooter__contentLeftLink">SKILLS</p></Link>
                    {/* <div className="newFooter__contentLeftDiv">
                        <div className="newFooter__contentLeftDivLine" />
                    </div> */}

                    {/* <div style={{ height: "15vh", borderRight: "1px solid black", width: "10px", margin: "15.2rem 0rem 5px 2rem" }} /> */}
                </div>
                {/* </Fade> */}

                <div className="newFooter__contentMiddle">
                    <div className="newFooter__contentIcons">
                        <a style={{ color: "inherit" }} href="mailto:pankajrohila1234@gmail.com?" target="_blank" rel="noopener noreferrer"><EmailIcon className="newFooter__contentIcon" /></a>
                        <a style={{ color: "inherit" }} href="https://bitbucket.org/Pankajrohila6688/" target="_blank" rel="noopener noreferrer"><GitHubIcon className="newFooter__contentIcon" /></a>
                        <a style={{ color: "inherit" }} href="https://www.linkedin.com/in/pankaj-rohila-65564616b/" target="_blank" rel="noopener noreferrer"><LinkedInIcon className="newFooter__contentIcon" /></a>
                    </div>

                    {/* <div className="newFooter__contentMiddleDiv" /> */}

                    <div className="newFooter__middleBottom">
                        {contact ? (
                            <div className="newFooter__contactForm">
                                <form onSubmit={sendEmail}>
                                    <div className="form__div">
                                        <input 
                                            required
                                            placeholder="Your name please"
                                            type="text"
                                            name="name"
                                            value={name}
                                            onChange={(e) => setName(e.target.value)}
                                        />

                                        <input 
                                            required
                                            type="email"
                                            placeholder="name@gmail.com"
                                            name="email"
                                            value={email}
                                            onChange={(e) => setEmail(e.target.valueAsDate)}
                                        />

                                        <textarea 
                                            required
                                            type="text"
                                            placeholder="Wanna say something?"
                                            name="message"
                                            value={message}
                                            onChange={(e) => setMessage(e.target.value)}
                                        />

                                        <button className="form__btn" type="submit">SEND A MAIL</button>
                                    </div>

                                </form>

                                <h6 className="copyright1">© 2021</h6>
                            </div>
                            
                        ) : (
                            <div className="newFooter__contactBtn">
                                <p>LET'S HAVE A TALK</p>
                                <button onClick={() => setContact(true)}>CONTACT ME</button>
                                <h6 className="copyright2">© 2021</h6>
                            </div>
                        )}
                    </div>

                </div>
                
                {/* <Fade right> */}
                <div className="newFooter__contentRight">
                    <Link spy={true} smooth={true} to="about"><p className="newFooter__contentRightLink">ABOUT</p></Link>
                    <Link spy={true} smooth={true} to=""><p onClick={() => history.push("/projects")} className="newFooter__contentRightLink">MY PROJECTS</p></Link>
                    {/* <div className="newFooter__contentRightDiv" /> */}
                    <Link spy={true} smooth={true} to="work"><p className="newFooter__contentRightLink">WORK</p></Link>
                    
                    {/* <div className="sideText__right">
                        <h6>pankajrohila1234@gmail.com</h6>
                        <div />
                    </div> */}
                </div>
                {/* </Fade> */}
            </div>
            </Fade>
        </div>
    )
}

export default Footer;



/* <form>
    <div className="form__div">
        <div className="form__divLeft">
            <h6 className="form__label">Name</h6>
            <input placeholder="Enter your name"/>
            <h6 className="form__label">Email</h6>
            <input placeholder="Enter your email"/>
        </div>
        <div className="form__divRight">
            <h6 className="form__label">Message</h6>
            <textarea placeholder="Enter your message"/>
        </div>
    </div>

    <button className="form__btn" type="submit">Send a message</button>
</form> */
