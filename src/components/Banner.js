import React from 'react';
import "./Banner.css";
// import HTMLFlipBook from "react-pageflip";
// import { useWindowSize } from 'react-use'
// import Confetti from 'react-confetti'
import Fade from 'react-reveal/Fade';
// import Bounce from 'react-reveal/Bounce';

const Banner = () => {
    // const { width, height } = useWindowSize();
    return (
        <div className="banner">
            {/* <Confetti
            width={width}
            height={height}
            /> */}
            {/* <div className="banner__left">
                <h3>PANKAJ ROHILA</h3>
                <h2>Designer<br />Web Developer<br /> & Creator</h2> 
                <p>
                    I'm a fourth year student persuing a degree in <br /> 
                    Computer Science Engineering. I am a self taught programmer <br />
                    and a web developer who create simple things beautifully.
                </p>
                <button class="banner__btn">RESUME</button>
            </div> */}
            <Fade bottom>
            <div className="banner__left">
                <h3 className="banner__h3">Hi, my name is</h3>
                <h2 className="banner__h2Top">Pankaj Rohila.</h2> 
                <h2 className="banner__h2Bottom">Designer & web developer.</h2>
                <p className="banner__p">
                    I'm a fourth year student pursuing a degree in <br /> 
                    Computer Science Engineering. I am a self taught programmer <br />
                    and a web developer who create simple things beautifully.
                </p>
                <a href="https://drive.google.com/drive/folders/1F8m8Xr4tiGnzxoTiYCoz1HwgUV_ZP6Wp?usp=sharing" target="_blank" rel="noopener noreferrer">
                    <button class="banner__btn">MY RESUME</button>
                </a>
            </div>
            </Fade>
        </div>
    )
}

export default Banner;