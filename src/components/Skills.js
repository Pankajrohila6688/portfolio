import React from 'react';
import "./Skills.css";
// import NewCard from '../cards/NewCard';
import SkillCard from '../cards/SkillCard';
// import Card from '../cards/Card';
// import WorkCard from '../cards/WorkCard';
// import Slide from 'react-reveal/Slide';
// import Fade from 'react-reveal/Fade';

function Skills() {
    return (
        <div className="skills">
            <div className="skills__heading">
                <h1>My Skills</h1>
            </div>

            <div className="skills__card">
                {/* <Slide left> */}
                {/* <Fade bottom> */}
                <SkillCard 
                    skillCard="skillCardRight" 
                    skillCardCard="rightCard" 
                    cardDetails="cardDetailsRight"
                    skillsLogo="skillsLogoRight"
                    title="Dev Tools"
                    tagline="Tools i work with while coding"
                    text="These are the tools that I mainly use while coding and building something creative"
                    image="/images/tools10.png"
                    text1="VS Code" text2="Figma" text3="Codepen" text4="Material UI" text5="Github"
                    logo1="/images/vs code.png" logo2="/images/figma.png" logo3="/images/codepen.png" logo4="/images/materialUI.png" logo5="/images/github.png"
                />
                {/* </Fade> */}
                {/* </Slide> */}
                
                {/* <Slide right> */}
                {/* <Fade bottom> */}
                <SkillCard 
                    skillCard="skillCardLeft" 
                    skillCardCard="leftCard" 
                    cardDetails="cardDetailsLeft"
                    skillsLogo="skillsLogoLeft"
                    title="Dev Skills"
                    tagline="Languages and frameworks"
                    text="These are the languages and libraries I mainly focused on and have built some projects based on these"
                    image="/images/devSkill10.png"
                    text1="JavaScript" text2="React" text3="Firebase" text4="HTML" text5="CSS"
                    logo1="/images/javascript.png" logo2="/images/react.png" logo3="/images/firebase.png" logo4="/images/html.png" logo5="/images/css.png"
                />
                {/* </Fade> */}
                {/* </Slide> */}

                {/* <Slide left> */}
                {/* <Fade bottom> */}
                <SkillCard 
                    skillCard="skillCardRight" 
                    skillCardCard="rightCard" 
                    cardDetails="cardDetailsRight"
                    skillsLogo="skillsLogoRight"
                    title="Side Skills"
                    tagline="What else i like to do"
                    text="When I'm not on the computer, I enjoy these things and more like reading about archaeological history and mythology of India"
                    image="/images/sideSkill10.png"
                    text1="Guitar" text2="Sketching" text3="Photography" text4="Cooking" text5="Basketball"
                    logo1="/images/guitar.png" logo2="/images/sketching.png" logo3="/images/camera.png" logo4="/images/cooking.png" logo5="/images/basketball.png"
                />
                {/* </Fade> */}
                {/* </Slide> */}
            </div>
        </div>
    )
}

export default Skills;

