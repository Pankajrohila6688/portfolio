import React, { useState } from 'react';
import './Header.css';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import Fade from 'react-reveal/Fade';
import { Link } from "react-scroll";
import { useHistory } from 'react-router-dom';
// import Scroll from 'react-scroll';
// import { NavLink } from "react-router-dom";


function Header() {

    const [menuItems, setMenuItems] = useState(false);
    const history = useHistory();

    return (
        <div className="header">
            <div className="header__left">
                {/* <h3>Logo</h3> */}
                <img className="logo" src="/images/logo500.png" alt="" onClick={() => history.push("./")} />
            </div>
            
            {menuItems ? (
                <div className="header__right">
                    <Fade>
                        <div className="header__rightLeft">
                            <Link className="link" spy={true} smooth={true} activeClassName="active" to="banner"><p>Home</p></Link>
                            <Link className="link" spy={true} smooth={true} activeClassName="active" to="about"><p>About</p></Link>
                            <Link className="link" spy={true} smooth={true} activeClassName="active" to="skills"><p>Skills</p></Link>
                            <Link className="link" spy={true} smooth={true} activeClassName="active" to="work"><p>Work</p></Link>
                            <Link className="link" spy={true} smooth={true} activeClassName="active" to="footer"><p>Contact</p></Link>
                        </div>
                        <div className="header__rightRight">
                            <span className="header__span" onClick={() => setMenuItems(false)}><CloseIcon className="header__closeIcon" /></span>
                        </div>
                    </Fade>
                </div>
            ) : (
                <div className="header__menu" onClick={() => setMenuItems(true)}>
                    <Fade>
                        <MenuIcon className="header__menuIcon"/>
                    </Fade>
                </div>
            )}
        </div>
    )
}

export default Header;
