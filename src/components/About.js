import React from 'react';
import "./About.css";
// import NewCard from '../cards/NewCard';
// import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';

function About() {
    // const w1 = "<html>"
    // const w2 = "<body>"
    // const w3 = "<p>"
    // const w4 = "</p>"
    // const w5 = "</body>"
    // const w6 = "</html>"

    return (
        // <Fade top>
        <div className="about">
            <Fade top>
            <div className="about__heading">
                <h1>About Me</h1>
                {/* <hr style={{ marginLeft: "37rem", marginRight: "37rem", marginTop: "10px", height: "2px" , backgroundColor: "purple", border: "none", borderRadius: "100px" }}/> */}
            </div>
            </Fade>

            {/* <Fade bottom> */}
            <div className="about__content">
                <Fade bottom>
                <div className="about__contentLeft">
                    <p>
                        {/* <span className="word1">{w1}</span> <br />
                        <span className="word2">{w2}</span> <br />
                        <br />
                        <span className="word3">{w3}</span> <br /> */}
                        {/* <Zoom> */}
                        {/* <p className="text">
                            I am a web developer, 
                            currently a fourth year student pursuing bachelor of technology in Computer Science.
                            I have a serious passion for creating intuitive, dynamic user experiences and web applications.<br />
                            I started programming with 'Python' language 3 years back, built some basic and simple applications and UI's for the web. 
                            Later switched to 'Javascript' for the better web development career and started building projects using 'React Js' and 'Firebase'.<br />
                            I aspire toward a career that will allow me to channel my creativity through crafting beautiful software and engaging experience. 
                            I'm interested in working on ambitious projects with positive people.<br />
                            When I'm not coding, I enjoy listening music, cooking, exploring new things on the internet related to archaeological history and mythology of India.<br /> 
                        </p> */}
                        <p className="text">
                            I am a web developer and a student pursuing my bachelor's degree in Computer Science. 
                            I have a serious passion for creating intuitive, dynamic user experiences and web applications.<br /><br />
                            I started programming with 'Python' back in 2018. My interest in designing and web development 
                            began after I built a few applications and GUIs using Python. Later, I picked up full stack development 
                            using JavaScript ecosystem and started building projects using it.<br /><br />
                            I aspire toward a career that will allow me to channel my creativity through 
                            crafting beautiful software and engaging experience. I'm interested in working 
                            on ambitious projects with positive people.<br />
                            When I'm not coding, I enjoy listening music, playing guitar, cooking and playing basketball.
                        </p>
                        {/* </Zoom> */}
                        {/* <span className="word3">{w4}</span> <br />
                        <br />
                        <span className="word2">{w5}</span> <br />
                        <span className="word1">{w6}</span> <br /> */}
                    </p>
                </div>
                </ Fade>
                
                <Fade bottom>
                <div className="about__contentRight">
                    {/* <Zoom> */}
                    {/* <img src="https://images.unsplash.com/photo-1629109078819-da344f157ebc?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzfHx8ZW58MHx8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" /> */}
                    <img src="/images/pic4.png" alt="" />
                    {/* </Zoom> */}
                    {/* <img src="/images/workImg1.png" alt="" /> */}
                    {/* <NewCard /> */}
                </div>
                </Fade>
            </div>
            {/* </Fade> */}
            <div className="about__fadeBottom" />
        </div>
        // </Fade>
    )
}

export default About
