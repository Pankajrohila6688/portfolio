import React from 'react';
import "./Testimonial.css";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import Fade from 'react-reveal/Fade';

function Testimonial() {

    const items = [
        <div className="text1">
           Pankaj is very passionate about technology. 
           He worked with me while we were building Asmi. 
           He quickly learnt full stack development and did end 
           to end implementation of a few key features of our product. 
           He is a very calm & composed person and a total team player.<br /><br />
           <p className="text1__span">~ Ashok Verma (Co-Founder, Asmi)</p>
           <p className="text1__span">At present - Project Manager, Accenture</p>
        </div>,
        <div className="text1">
            Pankaj Rohila has worked on a productivity app project with me. 
            During that time, Pankaj has been a self-starter and productive 
            contributor to team efforts, He has a really good attitude towards 
            learning and handles pressure really well. His skills, attributes, 
            and nature will play a huge role in a successful internship. 
            I wish him all the best.<br /><br />
            <p className="text1__span">~ Mahesh J (CTO, GradRight)</p>
        </div>
    ]

    const settings = {
        autoPlay: true,
        autoPlayInterval: 15000,
        mouseTracking: true,
        swipeable: true,
        animationDuration: 700,
        autoPlayStrategy: 'none',
        // disableButtonsControls: true, 
        infinite: true,
        keyboardNavigation: true,
        disableButtonsControls: true,
        disableDotsControls: false,
    }

    return (
        <div className="testimonial">
            <div className="testimonial__content">
                <Fade bottom>
                <div className="testimonial__text">
                    <h3 className="testimonial__heading">Testimonials for my previous work</h3>
                    <AliceCarousel {...settings} className="alice-carousel" items={items} />
                </div>
                </Fade>
            </div>
        </div>
    )
}

export default Testimonial;
