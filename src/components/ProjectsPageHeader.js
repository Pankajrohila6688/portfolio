import React from 'react';
import "./ProjectsPageHeader.css";
import { useHistory } from 'react-router-dom';

function ProjectsPageHeader() {
    const history = useHistory();

    return (
        <div className="projectsPageHeader">
            <img className="projectPageLeftImg" src="/images/workImg6.png" alt="" />
            <img className="projectPageLogo" src="/images/logo500.png" alt="" onClick={() => history.push("./")} />
            <img className="projectPageRightImg" src="/images/workRightImg.png" alt="" />
        </div>
    )
}

export default ProjectsPageHeader;
