import React from 'react';
import "./ProjectsPageFooter.css";
import GitHubIcon from '@material-ui/icons/GitHub';
// import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';

function ProjectsPageFooter() {
    return (
        <div className="projectsPageFooter">
            {/* <hr className="projectsPageLine"/> */}
            <div className="projectsFooterIcons">
            <a style={{ color: "inherit" }} href="mailto:pankajrohila1234@gmail.com?" target="_blank" rel="noopener noreferrer"><EmailIcon className="projectsFooterIcon" /></a>
            <a style={{ color: "inherit" }} href="https://bitbucket.org/Pankajrohila6688/" target="_blank" rel="noopener noreferrer"><GitHubIcon className="projectsFooterIcon" /></a>
            <a style={{ color: "inherit" }} href="https://www.linkedin.com/in/pankaj-rohila-65564616b/" target="_blank" rel="noopener noreferrer"><LinkedInIcon className="projectsFooterIcon" /></a>
            </div>
            {/* <div className="projectsPageLine"/> */}
            <div className="projectsFooterText">
                <h5>PANKAJ ROHILA</h5>
                <h6>© 2021</h6>
            </div>
        </div>
    )
}

export default ProjectsPageFooter;
