import React from 'react';
import "./Card.css";
import Card from 'react-animated-3d-card';

function SkillCard({ skill1, skill2, skill3, skill4, skill5 }) {
    return (
        <div className="card">
            <Card 
                style={{ 
                    // backgroundColor: '#cfd9df',
                    // backgroundImage: 'linear-gradient(#cfd9df , #e2ebf0)',
                    width: '300px',
                    height: '50vh',
                    // opacity: '0.9',
                    backgroundColor: "#cfd9df",
                    // marginLeft: '50px',
                    // marginTop: '50px',
                    // boxShadow: 'inset 6px 6px 10px 0 rgba(0, 0, 0, 0.1),inset -6px -6px 10px 0 rgba(255, 255, 255, 0.3)'
                    // boxShadow: '12px 12px 24px 0 rgba(0, 0, 0, 0.1),-12px -12px 24px 0 rgba(255, 255, 255, 0.4)'
                }}
                // isStatic = {true}
                cursorPointer = {false}
                borderRadius = "5px"
            >
            {/* <div className="card__heading">
                <h2 className="card__headingText">{title}</h2>
                <div className="card__headingLine" />
            </div> */}
            <div className="card__details">
                <h3>{skill1}</h3>
                <h3>{skill2}</h3>
                <h3>{skill3}</h3>
                <h3>{skill4}</h3>
                <h3>{skill5}</h3>
            </div>
        </Card>
        </div>
    )
}

export default SkillCard;



