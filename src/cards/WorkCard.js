import React from 'react';
import './WorkCard.css';

function WorkCard({ href, src, text }) {
    return (
        <div className="workCard">
            <a href={href} target="_blank" rel="noopener noreferrer"><img src={src} alt=""/></a>
            <p>{text}</p>
        </div>
    )
}

export default WorkCard;
