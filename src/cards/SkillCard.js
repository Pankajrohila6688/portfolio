import React from 'react';
// import Card from '../cards/Card';
import './SkillCard.css';
// import WorkCard from './WorkCard';
import Card from 'react-animated-3d-card';
import './Card.css';
// import Flip from 'react-reveal/Flip';
// import Slide from 'react-reveal/Slide';
// import Fade from 'react-reveal/Fade';
// import Zoom from 'react-reveal/Zoom';

function SkillCard({ skillCard, skillCardCard, cardDetails, skillsLogo, title, tagline, text, image, text1, text2, text3, text4, text5, logo1, logo2, logo3, logo4, logo5 }) {
    return (
        <div className={skillCard}>
            {/* <Fade> */}
            <div className="skillCard__left">
                {/* <Zoom > */}
                <div className="left">
                    {/* <Zoom> */}
                        <h1>{title}</h1>
                    {/* </Zoom> */}
                    {/* <Zoom> */}
                        <h2>{tagline}</h2>
                    {/* </Zoom> */}
                    {/* <Zoom> */}
                    <p>
                        {/* Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut aliquip ex ea commodo consequat. */}
                        {text}
                    </p>
                    {/* </Zoom> */}
                </div>
                {/* </Zoom> */}
                <div className="right">
                    <div className={skillCardCard}>
                        {/* <Card /> */}
                        <div className="card">
                        {/* <Flip left> */}
                        <Card 
                            style={{ 
                                // backgroundColor: '#cfd9df',
                                // backgroundImage: 'linear-gradient(#cfd9df , #e2ebf0)',
                                width: '300px',
                                // height: '50vh',
                                // opacity: '0.9',
                                backgroundColor: "#cfd9df",
                                // marginLeft: '50px',
                                // boxShadow: 'inset 6px 6px 10px 0 rgba(0, 0, 0, 0.1),inset -6px -6px 10px 0 rgba(255, 255, 255, 0.3)'
                                // boxShadow: '12px 12px 24px 0 rgba(0, 0, 0, 0.1),-12px -12px 24px 0 rgba(255, 255, 255, 0.4)'
                            }}
                            // isStatic = {true}
                            cursorPointer = {false}
                            borderRadius = "5px"
                        >
                        {/* <div className="card__heading">
                            <h2 className="card__headingText">{title}</h2>
                            <div className="card__headingLine" />
                        </div> */}
                        <div className={cardDetails}>
                            <h3>{text1} <img className={skillsLogo} src={logo1} alt="" /></h3>
                            <h3>{text2} <img className={skillsLogo} src={logo2} alt="" /></h3>
                            <h3>{text3} <img className={skillsLogo} src={logo3} alt="" /></h3>
                            <h3>{text4} <img className={skillsLogo} src={logo4} alt="" /></h3>
                            <h3>{text5} <img className={skillsLogo} src={logo5} alt="" /></h3>
                        </div>
                    </Card>
                    {/* </Flip> */}
                    </div>

                    </div>
                </div>
            </div>  
            {/* </Fade> */}

            {/* <Zoom> */}
            {/* <Fade> */}
            <div className="skillCard__right">
                {/* <Zoom> */}
                <img src={image} alt="" />
                {/* </Zoom> */}
            </div>
            {/* </Fade> */}
            {/* </Zoom> */}
        </div>
    )
}

export default SkillCard;
