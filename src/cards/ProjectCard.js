import React from 'react';
import './ProjectCard.css';
// import LinkIcon from '@material-ui/icons/Link';
// import GitHubIcon from '@material-ui/icons/GitHub';

function CardLeft({ projectCard, cardDetail, cardHeading, cardLang, cardLogo, projectName, projectDetail, lng1, lng2, lng3, projectImage, githubLink, liveDemo }) {
    return (
        <div className={projectCard}>
            <div className="projectCard__content">
                <div className="projectCard__image">
                    {/* <h1>image</h1> */}
                    <img src={projectImage} alt="" />
                </div>
                <div className="projectCard__details">
                    <div className={cardHeading}>
                        <p>Featured Project</p>
                        <h3>{projectName}</h3>
                    </div>
                    <div className={cardDetail}>
                        <p>
                            {projectDetail}
                        </p>
                    </div>
                    <div className={cardLang}>
                        <p>{lng1}</p>
                        <p>{lng2}</p>
                        <p>{lng3}</p>
                    </div>
                    <div className={cardLogo}>
                        {/* <button><GitHubIcon /> Code</button>
                        <button><LinkIcon /> Demo</button> */}
                        <a href={githubLink} target="_blank" rel="noopener noreferrer">
                            {/* <button className="cardLogoDiv1">
                                <p className="gitText">code</p> */}
                                <img className="gitLogo" src="/images/bit.png" alt="" />
                            {/* </button> */}
                        </a>

                        <a href={liveDemo} target="_blank" rel="noopener noreferrer">
                            {/* <button className="cardLogoDiv2">
                                <p className="linkText">demo</p> */}
                                <img className="linkLogo" src="/images/link.png" alt="" />
                            {/* </button> */}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardLeft;
