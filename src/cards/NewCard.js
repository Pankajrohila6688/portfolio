import React from 'react';
import './NewCard.css';

function NewCard() {
    return (
        <div className="newCard">
            <div className="img-container">
                <div className="img-inner">
                    <div className="inner-skew">
                        <img src="https://images.unsplash.com/photo-1629109078819-da344f157ebc?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzfHx8ZW58MHx8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                    </div>
                </div>
            </div>
            <div className="text-container">
                <h3>Tools</h3>
                <div>
                    {/* This a demo experiment to skew image container. It looks good. */}
                    <p>HTML</p>
                    <p>CSS</p>
                    <p>React</p>
                    <p>Redux</p>
                    <p>Firebase</p>
                </div>
            </div>
        </div>
    )
}

export default NewCard;
